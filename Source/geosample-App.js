var viewer = new Cesium.Viewer('cesiumContainer');
var camera = viewer.camera;
var ellipsoid = viewer.scene.globe.ellipsoid;

var baseUrl = 'https://dev.virtualearth.net/REST/v1/Locations/';

function setAddressPoint(position, locationAddress, longitudeDegrees, latitudeDegrees) {
    viewer.entities.add({
        name: 'Longitude:' + longitudeDegrees.toFixed(4) + ', Latitude: ' + latitudeDegrees.toFixed(4),
        position: position,
        point: {
            pixelSize: 5,
            color: Cesium.Color.RED,
            outlineColor: Cesium.Color.WHITE,
            outlineWidth: 2
        },
        label: {
            text: locationAddress,
            font: '14pt monospace',
            style: Cesium.LabelStyle.FILL_AND_OUTLINE,
            outlineWidth: 2,
            verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
            pixelOffset: new Cesium.Cartesian2(0, -5)
        }
    });
}

var handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
handler.setInputAction(function(click) {
    var position = camera.pickEllipsoid(click.position);
    if (Cesium.defined(position)) {
        var cartographic = ellipsoid.cartesianToCartographic(position);
        var longitudeDegrees = Cesium.Math.toDegrees(cartographic.longitude);
        var latitudeDegrees = Cesium.Math.toDegrees(cartographic.latitude);
        var url = baseUrl + latitudeDegrees + "," + longitudeDegrees;
        var resource = new Cesium.Resource({
            url: url,
            queryParameters: {
                key: Cesium.BingMapsApi.getKey()
            }
        });
        var promise = resource.fetchJsonp('jsonp');

        promise.then(function(result) {
            var resources = result.resourceSets[0].resources;
            if (resources.length === 0) {
                return;
            }
            var locationAddress = resources[0].name;
            if (locationAddress.length > 15) {
                locationAddress = locationAddress.replace(/, /g, '\n');
            }
            setAddressPoint(position, locationAddress, longitudeDegrees, latitudeDegrees);
        }).otherwise(function(error) {
            console.error("ERROR getting address: " + error);
        });
    }
}, Cesium.ScreenSpaceEventType.LEFT_CLICK);