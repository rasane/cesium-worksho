Cesium.BingMapsApi.defaultKey = "At8BueRsN-MKqWT6hIA9TKUiRehRwmPsi3xB2oy3hKazy_5eKYzPnG0X5S8c0Bfv";



var viewer = new Cesium.Viewer('cesiumContainer', {
    //terrainProvider: Cesium.createWorldTerrain()
});
// Add a WMS imagery layer
var imageryLayers = viewer.imageryLayers;

imageryLayers.addImageryProvider(new Cesium.BingMapsImageryProvider({
    url: 'https://dev.virtualearth.net',
    key: 'At8BueRsN-MKqWT6hIA9TKUiRehRwmPsi3xB2oy3hKazy_5eKYzPnG0X5S8c0Bfv',
    mapStyle: Cesium.BingMapsStyle.AERIAL_WITH_LABELS
}));

// No depth testing against the terrain to avoid z-fighting
viewer.scene.globe.depthTestAgainstTerrain = false;

var sceneJsonUrl = "honeywell-building.small/Scene/cesiummtilemhoneywell.json"
var cesiummtilemhoneywell = new Cesium.Cesium3DTileset({
    url: sceneJsonUrl,
    maximumScreenSpaceError: 1, // Temporary workaround for low memory mobile devices - Increase maximum error to 8.
    maximumNumberOfLoadedTiles: 1000 // Temporary workaround for low memory mobile devices - Decrease (disable) tile cache.
});
viewer.scene.primitives.add(cesiummtilemhoneywell);

cesiummtilemhoneywell.readyPromise.then(function(){
    console.log("model is now loaded !!!!")
    var cartographic = Cesium.Cartographic.fromCartesian(cesiummtilemhoneywell.boundingSphere.center);
    var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
    var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, -34.57435916);
    var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
    cesiummtilemhoneywell.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
});

// cesiummtilemhoneywell.tileVisible.addEventListener(function(tile) {
//     var content = tile.content;
//     var featuresLength = content.featuresLength;
//     for (var i = 0; i < featuresLength; i+=2) {
//         content.getFeature(i).color = Cesium.Color.fromRandom();
//     }
// });

var someRoomModelurl = "chiller-room-minus180x-minus3-8y.obj/gltf/gltf.gltf";
//+ 34.57435916

var cartesianCoordinate = Cesium.Cartesian3.fromDegrees(151.145417668, -33.7975321658, 23.637526958899997  + 12.9);
var modelMatrix = Cesium.Transforms.northUpEastToFixedFrame(cartesianCoordinate);
// var modelMatrix = Cesium.Transforms.eastNorthUpToFixedFrame(cartesianCoordinate);
var room = Cesium.Model.fromGltf({
    id: 'room',
    url: someRoomModelurl,
    modelMatrix: modelMatrix, 
    scale: 1.0,
    shadows: false,
    show: true
});
room = viewer.scene.primitives.add(room);
room.readyPromise.then(function(){
    console.log("gltf loaded!!");
    // room.color = getColor(object.color, 1.0);
});
var slider = document.getElementById("heightSliderLabel");
slider.oninput = function(){
    room.color = Cesium.Color.fromAlpha(room.color, parseFloat(1- this.value));
    //cesiummtilemhoneywell.color = Cesium.Color.fromAlpha(cesiummtilemhoneywell.color, parseFloat(this.value));
    var alpha = parseFloat(this.value);
    cesiummtilemhoneywell.style = new Cesium.Cesium3DTileStyle({
            "color" : "rgba(${red}, ${green}, ${blue}, (alpha))"
    });

}

var travelTo = document.getElementById("travelTo");
travelTo.addEventListener("change", function(e){
    var flyToOptions;
    if(travelTo.selectedIndex){
        flyToOptions = {
            "latitude": -33.79759584901937,
            "longitude": 151.14546018249766,
            "height": 25.81876283035402+ 12.9,
            "heading": 0.9434012720805818,
            "pitch": -0.42359454851579903,
            "roll": 6.280433783942186
        };

    }else{
        flyToOptions = {
            latitude: -33.79923244905671, longitude: 151.14622193469754, height: 108.98702518868183,
            heading: 5.830885702974744, pitch: -0.4594696642161171, roll: 0.001509990631860525, "duration": 7
        };
    }

    viewer.camera.flyTo({
        destination: Cesium.Cartesian3.fromDegrees(flyToOptions.longitude, flyToOptions.latitude, flyToOptions.height),
        duration: flyToOptions.duration || 5,
        easingFunction: Cesium.EasingFunction.LINEAR_NONE,
        pitchAdjustHeight: 1000,
        // maximumHeight: 1000,
        orientation: {
            heading: flyToOptions.heading,
            pitch: flyToOptions.pitch,
            roll: flyToOptions.roll
        }
        
    });
});


/* var object = Cesium.Model.fromGltf({
    id: 'object',
    url: 'sampledata/object/object.gltf',
    modelMatrix: modelMatrix, 
    scale: 10.0,
    shadows: false,
    show: true
});*/
// function getColor(color, alpha) {
//     return Cesium.Color.fromAlpha(color, parseFloat(alpha));
// }

// object = viewer.scene.primitives.add(object);
// object.readyPromise.then(function(){
//     object.color = getColor(object.color, 0.0);
// });

var boundingSphere = new Cesium.BoundingSphere(Cesium.Cartesian3.fromDegrees(151.1455032, -33.79784162, 34.57435916), 410.8931465);
viewer.camera.flyToBoundingSphere(boundingSphere);
